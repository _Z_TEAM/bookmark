# Bookmark

Our Bookmark

* [Free Tools For Dev ](#DevTools)
* [Algorithms ](#Algo)
* [Megadrive Dev ](#Megadrive)
* [PC EngineDev ](#PCEngine)
* [Langages ](#langages)
* [Game Source Code ](#Gamesource)
* [GFX Tools](#GFXTools)
* [Other ](#Other)

## DevTools
* [free-for-dev](https://github.com/alekmaul/free-for-dev/blob/master/README.md)

## Algo
* Détection de collisions en 2D (https://developer.mozilla.org/fr/docs/Games/Techniques/2D_collision_detection/)
* Collision-detection-using-an-axis-aligned-bounding-box (https://www.gamefromscratch.com/post/2012/11/26/GameDev-math-recipes-Collision-detection-using-an-axis-aligned-bounding-box.aspx)
* 2D Collision Detection (http://blog.sklambert.com/html5-canvas-game-2d-collision-detection/)
* Sprite Collision for Genesis (http://gendev.spritesmind.net/page-collide.html)
* The guide to implementing 2D platformers (http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/)
* Street Fighter II Hitboxes (https://combovid.com/?p=956)
* Camera_Logic_in_a_2D_Platformer (https://www.gamasutra.com/blogs/JochenHeizmann/20171127/310386/Camera_Logic_in_a_2D_Platformer.php)
* Tweening simulation (http://gizma.com/easing/)
* Game Programming Patterns (http://gameprogrammingpatterns.com/contents.html)
* Visualising Data Structures and algo (https://visualgo.net/en)
* Procedurally Generated Content (http://www.squidi.net/three/c_procedural.php)
* Tutoriels sur la théorie des collisions (https://jeux.developpez.com/tutoriels/theorie-des-collisions/)
* How to build a racing game (https://codeincomplete.com/posts/javascript-racer-v1-straight/)
* Lou's Pseudo 3d( http://www.extentofthejam.com/pseudo/)

## Megadrive
* Project MD's 3D mirror effect (https://www.gamasutra.com/blogs/JavierDegirolmo/20160202/264791/)
* How to optimize hblank, vblank, loop ? (http://gendev.spritesmind.net/forum/viewtopic.php?f=2&t=1721&hilit=play+anim)
* Tuto ASM tutorial Megadrive (https://huguesjohnson.com/programming/)
* Megadrive mapping manipulator (https://github.com/kirjavascript/Flex2)
* Plutiedev (good reference) (https://plutiedev.com/)
* Tuto SGDK Dnibus Games (https://danibus.wordpress.com/)
* Tuto SGDK OHSAT Games (https://www.ohsat.com/tutorial/)
* Continuous-delivery Megadrive (https://blog.roberthargreaves.com/2019/12/28/continuous-delivery-on-the-sega-mega-drive)
* Graphics Megadrive (https://rasterscroll.com/mdgraphics/)
* loading compressed csv (https://barelyconsciousgames.blogspot.com/2020/12/loading-rle-compressed-csv-tile-maps.html)
* Mode5 (https://mode5.net/index.html)
* HuguesJohnson retrogaming (https://huguesjohnson.com/programming/)
* SGDK Tutorial ( https://under-prog.ru/en/)

## PCEngine
* Forum PCEngineDev (http://pcedev.blockos.org/viewforum.php?f=5&sid=fde76d0bb25e605782099bf6030ccbbe)
* aetherbyte-squirrel(http://www.aetherbyte.com/aetherbyte-squirrel_for_pc-engine_and_turbografx-16.html)
* HUC Funtions/ PCEAS (Assembler) (http://archaicpixels.com/Main_Page)
* Emulateur Magic Engine (http://www.magicengine.com)
* Forum PC Engine (https://pcengine.proboards.com/thread/218/elmers-engine-programming-resource-links)
* Divers Src Bjorn Nah (https://github.com/bjorn-nah)
* Hardware Kits (http://www.tailchao.com/TurboGrafx/index.php)
* ASM Debug Tips (https://blog.blockos.org/?tag=pc-engine)
* Pcedev Blog (https://pcedev.wordpress.com)
* HuC6280 Assembly programming for the PC Engine!(https://www.chibiakumas.com/6502/pcengine.php)
* PC-Engine (TurboGrafx 16) System-specific information for cc65 (https://cc65.github.io/doc/pce.html#s3)
* Tools Image2PCE (http://somanybits.com/i2pce/index.html)
* Chris Covell's Creations (http://www.chrismcovell.com/creations.html)
* PC ENGINE WORLD (http://pce.cabbage.cx/)
* OrionSoft PCEngine (http://onorisoft.free.fr/retro.htm)
* Tuto overlay CD (http://bfg-gamepassion.blogspot.com/2012/09/tutorial-overlay-date-et-images-sur.html)
* Hack PCEngine mini (https://honeylab.hatenablog.jp/

## Langages
* Fonction inline en C (https://cpp.developpez.com/faq/cpp/?page=Les-fonctions-inline)
* Divers langages programmation Amiga http://obligement.free.fr/programmation.php)

## Gamesource / Reverse Engineering
* Aladdin Megadrive version (https://gamehistory.org/aladdin-source-code/)
* NES/GENESIS/SNES Games SourceCode (http://shrigley.com/source_code_archive/)
* Kroah's Game Reverse Engineering (http://bringerp.free.fr/RE/)
* Jotd Bitmap Brothers Remake (https://jotd.pagesperso-orange.fr/)
* FABIEN SANGLARD (http://fabiensanglard.net/)
* CODETAPPER'S AMIGA (https://codetapper.com/)
* Game Engine rewrite cyxdown (http://cyxdown.free.fr)
* Retro Reversing (https://www.retroreversing.com/)
* Retro illusion (https://illusion.64history.net/releases/)

## GFXtools
* Image Alpha (OSX) (https://pngmini.com/)
* Magic Tools -list of  game/gfx tools - (https://github.com/ellisonleao/magictools#spritesheet-tools)
* Reference Images (https://www.pureref.com/)
* Hd-index-painting-in-photoshop( http://danfessler.com/blog/hd-index-painting-in-photoshop)
* Color quantizer (https://www.softpedia.com/get/Multimedia/Graphic/Graphic-Others/Color-quantizer.shtml)
* DarkFunction Editor Sprite Animator (http://darkfunction.com/editor/)
* PixelArt French  (https://www.pixelart.fr/)
* Storyboarder (https://wonderunit.com/storyboarder/)

## Other
* Code Wheel (https://www.oldgames.sk/codewheel/)
* Genesis VGM Player (https://www.inphonik.com/products/rymcast-genesis-vgm-player/)
* Emulators written in JavaScript( https://www.cambus.net/emulators-written-in-javascript/)
* IDE Emulateur Console 8bit online ( https://8bitworkshop.com/v3.5.0/?file=mode4test.c&platform=sms-sms-libcv)
* Atari Game Tools (https://bitbucket.org/d_m_l/agtools/src/master/)
* PSX SDK (https://github.com/Lameguy64/PSn00bSDK)

## Hardware
* Cartridge & Flash (https://retrostage.net/)
* RetroPC1010 (https://www.movesystems.com.au/)
* Color Maximite 2 (http://geoffg.net/maximite.html)

## live coding
https://github.com/toplap/awesome-livecoding
https://livecoding.fr/ressources

